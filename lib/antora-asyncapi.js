'use strict'

const expandPath = require('@antora/expand-path-helper')
const userRequire = require('@antora/user-require-helper')
const { promises: fsp } = require('fs')
const { Environment } = require('nunjucks')
const parse = require('@asyncapi/parser').parse
const get = require('got')
const picomatch = require('picomatch')

const DEFAULT_TEMPLATE_PACKAGE = '@djencks/asyncapi-asciidoc-template'
const DEFAULT_NAV_INDEX = 10
const URI_SCHEME_RX = /^https?:\/\//

module.exports.register = function (generatorContext, { config }) {
  const logger = generatorContext.getLogger('@djencks/antora-asyncapi')
  config.logLevel && (logger.level = config.logLevel)
  // const debug = logger.isLevelEnabled('debug')
  const trace = logger.isLevelEnabled('trace')

  trace && logger.trace(config)

  //config is from playbook entry for this extension.
  // Config contains a queries array of component/version/module filters and picomatch relative matches.
  const queries = config.queries || (config.urls ? [] : [{}])
  // Config contains a urls array of url/component/version/module/relative specifications.
  const urls = config.urls || []
  const bodyByTag = 'bodyByTag' in config
  const renderToc = 'renderToc' in config
  const generateNav = 'generateNav' in config
  const navByTag = 'navByTag' in config
  const tocByTag = 'tocByTag' in config
  const timing = 'timing' in config

  const logGeneratedAsciidoc = 'logGeneratedAsciidoc' in config

  const customTemplate = config.template

  const params = { renderToc, tocByTag, bodyByTag }

  generatorContext.on('contentClassified',
    async ({ playbook, contentCatalog }) => {
      const playbookDir = playbook.dir || '.'
      const userRequireContext = { dot: playbookDir, paths: [playbookDir || '', __dirname] }
      const { filters, loader } = customTemplate
        ? userRequire(customTemplate, userRequireContext)
        : require(DEFAULT_TEMPLATE_PACKAGE)

      trace && logger.trace({ 'filter names': Object.keys(filters) })
      const env = new Environment(loader, { dev: true })
      Object.entries(filters).forEach(([name, fn]) => env.addFilter(name, fn))

      const beforeLoad = Date.now()
      timing && logger.info(`loading generator took ${Date.now() - beforeLoad} ms`)

      const pages = Object.values(queries.reduce((accum, query) => {
        query = Object.assign({}, query, { family: 'example' })
        const relative = query.relative
        const relCompare = relative ? (page) => picomatch(relative)(page.src.relative) : () => true
        delete query.relative
        const navIndex = query.navIndex || DEFAULT_NAV_INDEX
        delete query.navIndex
        contentCatalog.findBy(query).filter(relCompare).forEach((file) => {
          const key = generateKey(file)
          if (!(key in accum)) accum[key] = [file, navIndex]
        })
        return accum
      }, {}))
      const beforeGenerateAll = Date.now()
      await Promise.all([
        Promise.all(pages.map(async ([source, navIndex]) => {
          const src = Object.assign({}, source.src, { family: 'page', mediaType: 'text/asciidoc' })
          src.relative = src.relative.slice(0, src.relative.length - src.extname.length) + '.adoc'
          src.extname = '.adoc'
          const dirname = source.dirname
          const path = source.path
          const sourceString = source.contents.toString()
          return await generate(sourceString, src, dirname, path, contentCatalog, navIndex)
        })),
        Promise.all(urls.map(
          async ({ url, component, version, module: module_, relative, navIndex = DEFAULT_NAV_INDEX }) => {
            if (contentCatalog.getComponentVersion(component, version)) {
              try {
                const src = {
                  component,
                  version,
                  module: module_,
                  relative,
                  family: 'page',
                  mediaType: 'text/asciidoc',
                }
                const dirname = `modules/${module_}/pages`
                const path = `modules/${module_}/pages/${relative}`
                let source
                if (isUrl(url)) {
                  source = await get(url, { resolveBodyOnly: true, responseType: 'buffer' })
                } else {
                  const localPath = expandPath(url, '~+', playbookDir)
                  source = await fsp.readFile(localPath)
                }
                return await generate(source.toString(), src, dirname, path, contentCatalog, navIndex)
              } catch (error) {
                return logger.warn({ msg: `could not read asyncapi document from url ${url}`, error })
              }
            } else {
              logger.info(`No component ${component} at version ${version} for url ${url}: skipping`)
            }
          })),
      ])
      timing && logger.info(`total generation time ${Date.now() - beforeGenerateAll}`)

      async function generate (sourceString, src, dirname, path, contentCatalog, navIndex) {
        try {
          const beforeGenerate = Date.now()
          const parsed = await parse(sourceString)
          timing && logger.info(`parsing ${generateKey(src)} took ${Date.now() - beforeGenerate} ms`)
          const asciidoc = env.render('template/asyncapi.adoc', {
            asyncapi: parsed,
            params,
          })
          const page = {
            src,
            dirname,
            path,
            mediaType: 'text/asciidoc',
            contents: Buffer.from(asciidoc),
          }
          if (logGeneratedAsciidoc) {
            logger.info(`asciidoc page generated for ${generateKey(src)}:\n${asciidoc}`)
          }
          if (generateNav) {
            const navAsciidoc = env.render('template/nav.adoc', {
              asyncapi: parsed,
              params: {
                prefix: `${src.version}@${src.component}:${src.module}:${src.relative}`,
                outFilename: '',
                navByTag,
              },
            })

            if (logGeneratedAsciidoc) {
              logger.info(`asciidoc nav generated for ${generateKey(src)}:\n${navAsciidoc}`)
            }
            const nav = {
              src: Object.assign({}, src, { family: 'nav' }),
              dirname,
              path,
              mediaType: 'text/asciioc',
              contents: Buffer.from(navAsciidoc),
              nav: { index: navIndex },
            }
            contentCatalog.addFile(nav)
          }
          timing && logger.info(`generating ${generateKey(src)} took ${Date.now() - beforeGenerate} ms`)
          return contentCatalog.addFile(page)
        } catch (error) {
          logger.warn({ msg: `could not generate asyncapi document ${generateKey(src)}`, error })
        }
      }
    }
  )
}

function generateKey ({ component, version, module: module_, family, relative }) {
  return `${version}@${component}:${module_}:${family}$${relative}`
}

function isUrl (string) {
  return ~string.indexOf('://') && URI_SCHEME_RX.test(string)
}

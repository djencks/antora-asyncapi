/* eslint-env mocha */
'use strict'

const antoraAsyncapi = require('./../lib/antora-asyncapi')
const mockContentCatalog = require('./mock-content-catalog')
const { expect } = require('chai')

const seed = [
  {
    relative: 'page.adoc',
    contents: `= Fabulous page
    
    Once upon a time,
`,
  },
]

describe('antora-asyncapi tests', () => {
  var generatorContext
  let log

  beforeEach(() => {
    generatorContext = { on: (name, funct) => (generatorContext[name] = funct) }
    log = []
    generatorContext.getLogger = (name) => {
      return {
        trace: (msg) => {
          log.push({ level: 'trace', msg })
        },
        debug: (msg) => {
          log.push({ level: 'debug', msg })
        },
        info: (msg) => {
          log.push({ level: 'info', msg })
        },
        warn: (msg) => {
          log.push({ level: 'warn', msg })
        },
        isLevelEnabled: (level) => true,
      }
    }
  })

  it('validation error', async () => {
    antoraAsyncapi.register(generatorContext, { config: {} })
    const contentCatalog = mockContentCatalog([
      {
        family: 'example',
        relative: 'test.yml',
        contents: `asyncapi: '2.0.0'

externalDocs:
  description: Find more info here
  url: https://www.asyncapi.com

info:
  title: Dummy example with all spec features included
  version: '0.0.1'
  description: |
    This is an example of AsyncAPI specification file that is suppose to include all possible features of the AsyncAPI specification. Do not use it on production.

    It's goal is to support development of documentation and code generation with the [AsyncAPI Generator](https://github.com/asyncapi/generator/) and [Template projects](https://github.com/search?q=topic%3Aasyncapi+topic%3Agenerator+topic%3Atemplate)
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0
  contact:
    name: API Support
    url: http://www.asyncapi.com/support
    email: info@asyncapi.io
  x-twitter: '@AsyncAPISpec'

`,
      },
    ])
    await generatorContext.contentClassified({ playbook: {}, contentCatalog })
    expect(log.length).to.equal(3)
    expect(log[0].level).to.equal('trace')
    expect(log[1].level).to.equal('trace')
    expect(log[2].msg.msg).to.contain('could not generate asyncapi document master@component-a:module-a:page$test.adoc')
    expect(log[2].msg.error.title).to.equal('There were errors validating the AsyncAPI document.')
    expect(log[2].msg.error.validationErrors[0].title).to.equal('/ should have required property \'channels\'')
  })

  it('url test', async () => {
    antoraAsyncapi.register(generatorContext, {
      config: {
        urls: [{
          url: 'test/fixtures/asyncapi-test.yml',
          component: 'component-a',
          version: 'master',
          module: 'asyncapi',
          relative: 'asyncapi-test.adoc',
        }],
      },
    })
    const contentCatalog = mockContentCatalog(seed)
    await generatorContext.contentClassified({ playbook: {}, contentCatalog })
    expect(log.length).to.equal(2)
    expect(log[0].level).to.equal('trace')
    expect(log[1].level).to.equal('trace')
  }).timeout(5000)

  it('url no component-version test', async () => {
    antoraAsyncapi.register(generatorContext, {
      config: {
        urls: [{
          url: 'test/fixtures/asyncapi-test.yml',
          component: 'component-a',
          version: 'v2.0',
          module: 'asyncapi',
          relative: 'asyncapi-test.adoc',
        }],
      },
    })
    const contentCatalog = mockContentCatalog(seed)
    await generatorContext.contentClassified({ playbook: {}, contentCatalog })
    expect(log.length).to.equal(3)
    expect(log[0].level).to.equal('trace')
    expect(log[1].level).to.equal('trace')
    expect(log[2].level).to.equal('info')
    expect(log[2].msg).to.equal('No component component-a at version v2.0 for url test/fixtures/asyncapi-test.yml: skipping')
  }).timeout(5000)
})
